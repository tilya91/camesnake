import javax.swing.*;
import java.awt.*;

/**
 * Created by clest on 21.12.2016.
 */
// havatron is player

public class Havatron {
    int x;
    int y;
    Image image = new ImageIcon("img/havatron.jpg").getImage();
    int points = 0; //очки
    Food food = new Food();

    Controller controller = new Controller();

    public Havatron() {
    }

    public void move(){
        if (food.isEated(this)){
            food = new Food();
            if (Math.abs(x-food.x)<40 && Math.abs(y-food.y)<40) food = new Food();
        }
        controller.move();
        x = controller.getX();
        y = controller.getY();
    }

    public Image getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Controller getController() {
        return controller;
    }

    public void setPoints(){
        points++; // points = points+1;
    }

    public int getPoints() {
        return points;
    }

    public Food getFood() {
        return food;
    }
}


