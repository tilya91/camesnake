import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by clest on 21.12.2016.
 */
public class Adapter extends KeyAdapter {
    Controller controller;

    public Adapter(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        controller.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        controller.keyReleased(e);
    }


}
