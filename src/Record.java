import java.util.LinkedList;
import java.util.List;

/**
 * Created by clest on 22.01.2017.
 */
public class Record { //рекорды хранятся здесь
    int max;
    List<Integer> scores = new LinkedList<>();

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        if (max>this.max)
        this.max = max;
        scores.add(max);
    }
}
