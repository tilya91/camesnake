import javax.swing.*;

/**
 * Created by clest on 21.12.2016.
 */
public class Main {

    public Main() {
        start ();
    }

    public void start() {
        JFrame frame = new JFrame ();
        Room room = new Room ();

        frame.setTitle ("HavaTron");//название окна
        frame.setSize (315, 378); //размер окна
        frame.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo (null);
        frame.setVisible (true);
        frame.add (room);
    }


    public static void main(String[] args) {
        new Main();


    }
}