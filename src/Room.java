import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by clest on 21.12.2016.
 */
public class Room extends JPanel implements ActionListener{

    Timer timer = new Timer(10,this);
    Second time = new Second();
    Havatron havatron;
    Food food;
    Record record = new Record();


    public Room() { //зарисовка комнаты

        havatron = new Havatron();
        addKeyListener(new Adapter(havatron.getController()));
        timer.start();
        setFocusable(true);
        setDoubleBuffered(true);
        setSize(300,340);
        repaint();


    }

    @Override
    public void paint(Graphics g){

        super.paint(g);
        Graphics2D graphics2D = (Graphics2D)g;

        food = havatron.getFood(); //получить food
        if(time.getTime()==0){
            graphics2D.drawString(String.valueOf(havatron.getPoints()), 160, 165); //выведение балла на середину
            //graphics2D.drawString("Конец игры! Ваш балл:", 100, 150);
            record.setMax(havatron.getPoints());
        }
        else {
            graphics2D.drawImage(havatron.getImage(), havatron.getX(), havatron.getY(), this);

            if (food != null && !food.isEated(havatron)) {
                graphics2D.drawImage(food.getImage(), food.getX(), food.getY(), this);
            }
        }
//            graphics2D.drawLine(0,300,300,300);
//            graphics2D.drawLine(0,301,300,301);
//            graphics2D.drawLine(0,302,300,302);
//
//            graphics2D.drawLine(149,300,149,340);
//            graphics2D.drawLine(150,300,150,340);
//            graphics2D.drawLine(151,300,151,340);

//            graphics2D.drawLine(0,300,300,300);
//            graphics2D.drawLine(0,301,300,301);
//            graphics2D.drawLine(0,338,300,338);

//            graphics2D.drawLine(149,300,149,340);
//            graphics2D.drawLine(0,49,0,40);
           // graphics2D.drawLine(150,300,150,340);

            graphics2D.drawString(String.valueOf(havatron.getPoints()),120,324);
            graphics2D.drawString(String.valueOf(time.getTime()),270,324);

        Toolkit.getDefaultToolkit().sync();
        g.dispose();

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        havatron.move();
        repaint();
    }

    public Havatron getHavatron() { //получить хаватрона
        return havatron;
    }
}
