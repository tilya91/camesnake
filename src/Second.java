import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by clest on 22.01.2017.
 */
public class Second implements ActionListener{
    Timer timer = new Timer(1000,this);
    int time=10; //время для игры например 30 сек

    public Second() {
        timer.start();//
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        time--; //-30
        if (time==0)timer.stop(); //когда 0 == stop
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
