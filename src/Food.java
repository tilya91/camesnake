import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by clest on 16.01.2017.
 */
public class Food {

    Image image;
    int x;
    int y;
    boolean eated = false;

    public Food() {
        Random random = new Random();
        x = random.nextInt(251);
        y = random.nextInt(251);
        image = new ImageIcon("img/food1.png").getImage();
    }

    public Image getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isEated(Havatron havatron) {
        if (Math.abs(havatron.getX()-x) <= 15 && Math.abs(havatron.getY()-y) <= 15){
            if (!eated)havatron.setPoints();
            eated = true;
        }
        return eated;
    }
}
// |9| = 9  |-9| = 9