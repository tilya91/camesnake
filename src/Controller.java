import java.awt.event.KeyEvent;

/**
 * Created by clest on 21.12.2016.
 */

public class Controller {
    int x;
    int y;
    int dx;
    int dy;

    public Controller() {
        x = 0;
        y = 0;
    }

    public void move(){
        if((x+dx)>(-1) && x+dx<261) x+=dx;
        if(y+dy>-1 && y+dy<261) y+=dy;
    }


    public void keyPressed(KeyEvent e){

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_UP) dy=-1;
        if (key == KeyEvent.VK_DOWN)dy=1;
        if (key == KeyEvent.VK_LEFT)dx=-1;
        if (key == KeyEvent.VK_RIGHT) dx=1;
    }

    public void keyReleased(KeyEvent e){

        int key=e.getKeyCode();

        if (key == KeyEvent.VK_UP)dy=0;
        if (key == KeyEvent.VK_DOWN) dy=0;
        if (key == KeyEvent.VK_LEFT)dx=0;
        if (key == KeyEvent.VK_RIGHT)dx=0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}


